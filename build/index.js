"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getStudent = void 0;
const ajv_1 = __importDefault(require("ajv"));
const axios_1 = __importDefault(require("axios"));
const StudentSchema_json_1 = __importDefault(require("./StudentSchema.json"));
const ajv = new ajv_1.default();
const validate = ajv.compile(StudentSchema_json_1.default);
const getStudent = () => __awaiter(void 0, void 0, void 0, function* () {
    return axios_1.default
        .get('https://example.com')
        .then((res) => {
        const valid = validate(res.data);
        if (valid) {
            return res.data;
        }
        throw new Error('invalid response structure');
    })
        .catch((error) => {
        throw new Error(error);
    });
});
exports.getStudent = getStudent;
