"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollegeEnums = void 0;
var CollegeEnums;
(function (CollegeEnums) {
    CollegeEnums["AHSS"] = "Arts, Humanities & Social Sciences";
    CollegeEnums["HSPH"] = "Health Science & Public Health";
    CollegeEnums["PP"] = "Professional Programs";
    CollegeEnums["STEM"] = "Science, Technology, Engineering & Mathematics";
})(CollegeEnums = exports.CollegeEnums || (exports.CollegeEnums = {}));
