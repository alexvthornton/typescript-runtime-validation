# Typescript Runtime Validation

An Example project to demonstrate how to, using typescript types/interfaces, validate incoming requests/responses from web service calls at runtime.

## How it Works
We use [typescript-json-schema](https://github.com/YousefED/typescript-json-schema) to convert our existing typescript types and interfaces into JSON Schema. Then using [Ajv JSON Schema Validator](https://github.com/ajv-validator/ajv) we can validate that incoming JSON payloads match the expected structure.

## See it Work
To see it work, first, simply run `npm install` to add all the dependencies, then, run `npm run test`. This command first compiles the `Student` interface into a JSON Schema then runs the test `ObjectStructureValiation.test.ts`. Furthermore, if you run `npm run build` and take a look at `index.js` in the build directory you will see the validation check persists when the typescript is compiled into javascript.
## Test Cases
There are several tests written. The first shows that when the incoming JSON structure matches the expected structure the JSON payload can be converted to that type. The other tests show how when fields are missing, inaccurate, or are represented using the wrong type, they do not pass validation.

## Further Detail
To do that conversion from interface to JSON Schema the script `InterfaceToJsonSchema.sh` is ran. Taking a look at that file you will see the `typescript-json-schema` command that is run. Notice the `-o` flag, that is used to specify the output file. and the `--required` flag that is used to add a required array that lists the non-optional fields

## Converting more than one Interface/type to JSON Schema
To convert more than one interface/type to JSON Schema simply copy the command in `InterfaceToJsonSchema.sh` to a new line and change it to the interface/type you are targeting. Doing this ensures whenever the command `npm run build` or `npm run test` is ran all the interfaces/types specified in the file are converted to JSON Schema. This ensures changes made to those interfaces/types are always reflected in the JSON Schema.