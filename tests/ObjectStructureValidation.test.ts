import axios from 'axios';
import { getStudent } from '../src/index';
import validStudent from './validStudent.json';
import invalidStudent1 from './invalidStudent1.json';
import invalidStudent2 from './invalidStudent2.json';
import invalidStudent3 from './invalidStudent3.json';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('Testing object structure validation on read student', () => {
    test('Positive test valid student should return student object', async () => {
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: validStudent, status: 200 }));
        const result = await getStudent();
        expect(result).toEqual(validStudent);
    });
    test('Negative test invalid student enrolledCourses not array should throw error', async () => {
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: invalidStudent1, status: 200 }));
        await expect(getStudent()).rejects.toThrow(Error);
    });
    test('Negative test invalid student missing name should throw error', async () => {
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: invalidStudent2, status: 200 }));
        await expect(getStudent()).rejects.toThrow(Error);
    });
    test('Negative test invalid student college not enum should throw error', async () => {
        mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: invalidStudent3, status: 200 }));
        await expect(getStudent()).rejects.toThrow(Error);
    });
});
