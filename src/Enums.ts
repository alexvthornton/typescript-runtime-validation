export enum CollegeEnums {
    AHSS = 'Arts, Humanities & Social Sciences',
    HSPH = 'Health Science & Public Health',
    PP = 'Professional Programs',
    STEM = 'Science, Technology, Engineering & Mathematics',
}
