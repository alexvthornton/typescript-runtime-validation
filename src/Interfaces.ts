import * as Enums from './Enums';

export interface Student {
    name: string;
    college: Enums.CollegeEnums;
    enrolledCourses: Course[];
}

export interface Course {
    number: string;
    name: string;
}
