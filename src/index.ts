import Ajv from 'ajv';
import axios from 'axios';
import StudentSchema from './StudentSchema.json';
import { Student } from './Interfaces';

const ajv = new Ajv();
const validate = ajv.compile(StudentSchema);

export const getStudent = async (): Promise<Student> => {
    return axios
        .get('https://example.com')
        .then((res) => {
            const valid = validate(res.data);
            if (valid) {
                return res.data as Student;
            }
            throw new Error('invalid response structure');
        })
        .catch((error) => {
            throw new Error(error);
        });
};
